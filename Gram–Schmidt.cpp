#include <iostream>
#include <stdlib.h>
#include <vector>
#include <iomanip>
#include <cmath>
#include "mpi.h"

using namespace std;

int Print(vector<vector<double> > imatrix)
{
	int I=imatrix.size();
	int J=imatrix[0].size();
	for (int i=0; i<I; i++)
	{
		for (int j=0; j<J; j++ )
		{
			cout.precision(3);
			cout<<left << setw(7)<<round(imatrix[i][j]*100)/100;
		}
		cout<<endl;
	}
	cout<<'\n';
	return 0;
}

int Print(vector<double> imatrix)
{
	int I=imatrix.size();
	for (int i=0; i<I; i++)
	{
			cout.precision(3);
			cout<<left << setw(7)<<round(imatrix[i]*100)/100;
	}
	cout<<'\n';
	return 0;
}

void check(vector<vector<double> > A)
{
	double eps=0.0001;
	int dim=A.size();
	int fl=0;
    for (int i = 0; i < dim; ++i)
    {
        for (int j = 0; j < dim; ++j)
        {
            double sum = 0.;
            for (int k = 0; k < dim; ++k)
            {
                sum += A[i][k] * A[j][k];
            }
            if (i!=j)
            {
                if (abs(sum) > eps)
                {
                    cout << "Failed orthogonality: " << i << ", " << j << ", " << sum << endl;
                    fl=1;
                    break;
                }
            }
        }
    }
    if (fl==0) cout<<"Successful orthogonalization!"<<endl;
}

vector<double> Proj(vector<double> ai, vector<double> bi, int dim)
{
    double scAB=0;
    double scBB=0;
    double sc;
    for(int k=0;k<dim;k++)
    {
        scAB+=ai[k]*bi[k];
        scBB+=bi[k]*bi[k];
    }
    sc=scAB/scBB;
    for(int k=0;k<dim;k++)
        bi[k]*=sc;
    return bi;
}

int main(int argc, char* argv[])
{
    int dim;
    int num_dim=20;
    int num_proc_iter=10;
    MPI_Init(&argc, &argv);
    int procs_rank, procs_count, procs_count1;
    MPI_Comm_size(MPI_COMM_WORLD,&procs_count1);
    MPI_Comm_rank(MPI_COMM_WORLD,&procs_rank);
    int procs_count10=floor(procs_count1/num_proc_iter);
    if (procs_rank==0)
    {
        MPI_Status status;
        cout<<"start"<<endl;

        int dim_h=20;
        for (int i=1;i<procs_count1;i++)
        {
            MPI_Send( &num_dim, 1, MPI_INT, i , 10, MPI_COMM_WORLD);
        }
        //for (procs_count=2;procs_count<=20;procs_count+=1)
        for (procs_count=procs_count10;procs_count<=procs_count10*num_proc_iter;procs_count+=procs_count10)
        {
            for (int i=1;i<procs_count1;i++)
            {
                MPI_Send( &procs_count, 1, MPI_INT, i , 11, MPI_COMM_WORLD);
            }
            for (dim=dim_h;dim<=dim_h*num_dim;dim+=dim_h)
            {
                vector<vector<double> > a,b;
                // Заполнение
                for(int i = 0; i < dim; ++i)
                {
                    vector<double> tempA,tempB;
                    for(size_t j = 0; j < dim; ++j)
                    {
                        tempA.push_back(rand() % 50);
                        tempB.push_back(0);
                    }
                    a.push_back(tempA);
                    b.push_back(tempB);
                }

                //инициализация b
                for (int i=0;i<dim;i++)
                    for (int j=0;j<dim;j++)
                        b[i][j]=a[i][j];

                for (int i=1;i<procs_count;i++)
                {
                    MPI_Send( &dim, 1, MPI_INT, i , 2, MPI_COMM_WORLD);
                }
                double start, stop;
                start = MPI_Wtime();

                for (int j=0;j<dim;j++)
                {
                    vector<double> bi;
                    int kol[procs_count];
                    for (int i=0;i<procs_count;i++) kol[i]=0;
                    int num_of_pr=1;
                    for(int i=j+1;i<dim;i++)
                    {
                        if (num_of_pr>=procs_count)
                            num_of_pr=1;
                        kol[num_of_pr]+=1;
                        MPI_Send( &i, 1, MPI_INT, num_of_pr , 8, MPI_COMM_WORLD);
                        MPI_Send( &a[i][0], dim, MPI_DOUBLE, num_of_pr , 0, MPI_COMM_WORLD);
                        MPI_Send( &b[j][0], dim, MPI_DOUBLE, num_of_pr , 4, MPI_COMM_WORLD);
                        num_of_pr++;
                    }
                    for (int i=1;i<procs_count;i++)
                    {
                        MPI_Send( &kol[i], 1, MPI_INT, i, 1, MPI_COMM_WORLD);
                    }

                    for (int i=1;i<procs_count;i++)
                    {
                        double tmpB[dim];
                        for (int k=0;k<kol[i];k++)
                        {
                            int pr;
                            MPI_Recv( &pr, 1, MPI_INT , i, 9, MPI_COMM_WORLD, &status );
                            MPI_Recv( &tmpB, dim, MPI_DOUBLE , i, 5, MPI_COMM_WORLD, &status );
                            vector<double> tmp1;
                            for(int j=0;j<dim;j++)
                            {
                                tmp1.push_back(tmpB[j]);
                            }
                            for(int m=0;m<dim;m++)
                            {
                                b[pr][m]-=tmpB[m];
                            }
                        }
                    }
                    for (int i=1;i<procs_count;i++)
                    {
                        int wait;
                        MPI_Recv( &wait, 1, MPI_INT , i, 6, MPI_COMM_WORLD, &status );
                        MPI_Send( &wait, 1, MPI_INT, i, 7, MPI_COMM_WORLD);
                    }
                }
                stop = MPI_Wtime();
                cout << left << setw(7)<<round(round(1000000*(stop - start))/1000);
            }
            cout<<endl;
        }
    }
    if (procs_rank!=0)
    {
        MPI_Status status;
        int k, dim,num_dim,proc_c;
        for (int i=procs_rank;i<procs_rank+1;i++)
        {
            MPI_Recv( &num_dim, 1, MPI_INT , 0, 10, MPI_COMM_WORLD, &status );
            for (int y=0;y<num_proc_iter;y++)
            {
                MPI_Recv( &proc_c, 1, MPI_INT , 0, 11, MPI_COMM_WORLD, &status );
                if (procs_rank<proc_c)
                    for (int Dim=0;Dim<num_dim;Dim++)
                    {
                        MPI_Recv( &dim, 1, MPI_INT , 0, 2, MPI_COMM_WORLD, &status );
                        double a[dim];
                        for (int u=0;u<dim;u++)
                        {
                            MPI_Recv( &k, 1, MPI_INT , 0, 1, MPI_COMM_WORLD, &status );
                            for (int i=0;i<k;i++)
                            {
                                int l;
                                MPI_Recv( &l, 1, MPI_INT , 0, 8, MPI_COMM_WORLD, &status );
                                MPI_Recv( &a, dim, MPI_DOUBLE , 0, 0, MPI_COMM_WORLD, &status );
                                vector<double> ai,bi,bj;
                                for(int j=0;j<dim;j++)
                                {
                                    ai.push_back(a[j]);
                                }
                                MPI_Recv( &a, dim, MPI_DOUBLE , 0, 4, MPI_COMM_WORLD, &status );
                                for(int j=0;j<dim;j++)
                                {
                                    bj.push_back(a[j]);
                                }
                                vector<double> p=Proj(ai,bj,dim);
                                double bi1[dim];
                                for (int j=0;j<dim;j++)
                                    bi1[j]=p[j];
                                MPI_Send( &l, 1, MPI_INT, 0 , 9, MPI_COMM_WORLD);
                                MPI_Send( &bi1, dim, MPI_DOUBLE, 0 , 5, MPI_COMM_WORLD);
                            }
                            int wait;
                            MPI_Send( &wait, 1, MPI_INT, 0 , 6, MPI_COMM_WORLD);
                            MPI_Recv( &wait, 1, MPI_INT , 0, 7, MPI_COMM_WORLD, &status );
                        }
                    }
            }
        }
    }
MPI_Finalize();
return 0;
}